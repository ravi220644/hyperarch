#packetLoss
#ReadLatency
#WriteLatency
#ContextSwitch
#Page Swapin/ Swapout
import subprocess
s = ['PacketDropReceive','PacketDropTransmit','%IOWait','BlockSwapIn','BlockSwapOut','ContextSwitch']
val = []
f1 = open('/proc/net/dev','r')
for line in f1:
	arr = line.split()
	if len(arr)>10 and arr[1].isdigit() and arr[0].startswith('enp'):
		val.append(int(arr[4]))
		val.append(int(arr[12]))

proc = subprocess.Popen(['iostat','-x'],stdout=subprocess.PIPE)
while True:
  line = proc.stdout.readline()
  if line != '':
    if line.startswith('avg-cpu'):
    	line2 = proc.stdout.readline()
    	val.append(line2.split()[3])
  else:
    break

proc = subprocess.Popen(['vmstat'],stdout=subprocess.PIPE)
while True:
  line = proc.stdout.readline()
  if line != '':
    if line.split()[0].isdigit():
    	line = line.split()
    	val.append(line[6])
    	val.append(line[7])
    	val.append(line[11])
  else:
    break

i=0 
while i<len(s):
	print s[i],':',val[i]
	i = i+1

