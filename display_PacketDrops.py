import time
init_pktDropTransmit = 0
init_pktDropReceive = 0
fw = open('packets','w')
while True:
	val = []
	f1 = open('/proc/net/dev','r')
	for line in f1:
		arr = line.split()
		if len(arr)>10 and arr[1].isdigit() and arr[0].startswith('enp'):
			val.append((int(arr[4])-init_pktDropReceive)/100)
			val.append((int(arr[12])-init_pktDropTransmit)/100)
			init_pktDropReceive=int(arr[4])
			init_pktDropTransmit = int(arr[12])
	fw.write(str(val[0])+ ' ' + str(val[1]))
	time.sleep(100)