Hyperarch is a project based on QOS management in HyperCoverge architecture. Initial goal is to design VM placement and migration algorithm to maintain the QOS of the system. We have setup an initial testbed setup to simulate a small data center. All the files here are to test the vm placement algorithm designed by us on the testbed setup.

Files and its function :
--------------------------

cluster.csv
------------
This files contains the coordinates of the 6 cluster centers based on type of procees namely:
1. CPU Intensive
2. Network Intensive 
3. Disk Read Intensive
4. Disk Write Intensive
5. Memory Intensive
6. Idle

The cluster centers are in the order as given above for cluster.csv

parseXML.py
--------------
This has utility function, parseVM() which takes the filepath for usage statistics of VM and name of the file to which it will save parsed data which basically contains normalized usage data for each resource type from which filters below 14 important parameters in our classifiy_vm function in classify_vm.py which is explained in the next section. The 14 paramters that we consider are:
1. Read data rate
2. Write data rate
3. Read latency 
4. Write latency 
5. Flush latency 
6. Receive data rate
7. Transmit data rate 
8. Memory used
9. Memory Buffered
10. Memory Cached
11. Memory free
12. CPU used by user
13. CPU overhead
14. Total CPU used

classify_vm.py
---------------
This has a utility function, classify_vm() which takes the filepath for VM statistics (it will be explained in later section) as an input and returns two confidence vectors (confidence vectors is a vector of size 6, each field specifying the confidence value between 0 and 1 which shows how likely the VM belongs to that particular VM) based on absolute usage statistics and classified usage statistics respectively.

placevm.py
--------------
This file uses parseVM() to parse the usage statistics to generate parsed data which are saved as .csv files and these files are used by classify_vm() to generate the confidence vectors which will be used by the placement algorithm. To better know about the placement algorithm you can visit the presentation [here][1]

[1]: https://docs.google.com/presentation/d/1hCJm7Dn_8Nzz3VBrALk6ys6nn7GwMhG9MG4QCwdzsxM/edit?usp=sharing

displayQOS.py
---------------
This prints the parameters which indicated the QOS of the system, namely: 
1. PacketDropReceive
2. PacketDropTransmit
3. %IOWait
4. BlockSwapIn
5. BlockSwapOut
6. ContextSwitch
