import re,csv,os


vm = []

types = ['disk','nics','stats']

def parseVM(path, savefile):
	vm_temp = []
	flag=0
	for j in types:
		if os.path.exists(path + '/vm'+j):
			temp = []
			f = open(path + '/vm'+j,'r')
			count = 0
			for line in f:
				if re.search('xml version',line):
					if len(temp)!=0:
						if flag==0:
							vm_temp.append(temp)
						else:
							if count<len(vm_temp):
								vm_temp[count].extend(temp)
						count = count+1
					temp = []
				elif re.search('datum',line):
					temp.append(float(line.split('>')[1].split('<')[0]))
			if len(temp)!=0:
				if flag==0:
					vm_temp.append(temp)
				else:
					if count<len(vm_temp):
						vm_temp[count].extend(temp)
				count = count +1 
			flag=1
	try:
		somelen = len(vm_temp[0])
	except:
		print savefile,path
	vm_temp_1 = []
	for row in vm_temp:
		if len(row)==somelen:
			vm_temp_1.append(row)
	vm.append(vm_temp_1)

	count=0
	for i in vm:
		csvfile = open(savefile, 'wb',os.O_NONBLOCK)
		spamwriter = csv.writer(csvfile, delimiter=',',
	                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
		for j in i:
			spamwriter.writerow(j)
			csvfile.flush()
		csvfile.close()
		count = count+1