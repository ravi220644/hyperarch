from classify_vm import *
from parseXML import *
import numpy as np
vms_s1 = ['s1/vm1']
vms_s2 = ['s2/vm1']
vms_save_s1 = ['s1_vm1.csv']
vms_save_s2 = ['s2_vm1.csv'] 

i=0
while i<len(vms_s1):
	parseVM(vms_s1[i],vms_save_s1[i])
	i=i+1

i=0
while i<len(vms_s2):
	parseVM(vms_s2[i],vms_save_s2[i])
	i=i+1

parseVM('test/vm','test_vm.csv')
test_conf,test_conf2 = classify_vm('test_vm.csv')
conf_s1 = []
conf_s2 = []
confabs_s1 = []
confabs_s2 = []
num_s1 = 0.0
numabs_s1= 0.0
numabs_s2 = 0.0
num_s2 = 0.0

i=0
while i<len(vms_save_s1):
	vm_conf, vm_conf2 = classify_vm(vms_save_s1[i])
	for item in conf_s1:
		 num_s1 = num_s1 + (1.0/(1.0+np.linalg.norm(np.asarray(vm_conf,dtype=float)-np.asarray(item,dtype=float))))
	for item in confabs_s1:
		 numabs_s1 = numabs_s1 + (1.0/(1.0+np.linalg.norm(np.asarray(vm_conf2,dtype=float)-np.asarray(item,dtype=float))))
	conf_s1.append(vm_conf)
	confabs_s1.append(vm_conf2)
	i = i+1

for item in conf_s1:
	num_s1 = num_s1 + (1.0/(1.0+np.linalg.norm(np.asarray(test_conf,dtype=float)-np.asarray(item,dtype=float))))
for item in confabs_s1:
	numabs_s1 = numabs_s1 + (1.0/(1.0+np.linalg.norm(np.asarray(test_conf2,dtype=float)-np.asarray(item,dtype=float))))


num_s1 = 2*num_s1 / ((len(conf_s1)+1)*(len(conf_s1)))
numabs_s1 = 2*numabs_s1 / ((len(confabs_s1)+1)*(len(confabs_s1)))


i=0
while i<len(vms_save_s2):
	vm_conf, vm_conf2 = classify_vm(vms_save_s2[i])
	for item in conf_s2:
		 num_s2 = num_s2 + (1.0/(1.0+np.linalg.norm(np.asarray(vm_conf,dtype=float)-np.asarray(item,dtype=float))))
	for item in confabs_s2:
		 numabs_s2 = numabs_s2 + (1.0/(1.0+np.linalg.norm(np.asarray(vm_conf2,dtype=float)-np.asarray(item,dtype=float))))
	conf_s2.append(vm_conf)
	confabs_s2.append(vm_conf2)
	i = i+1

for item in conf_s2:
	num_s2 = num_s2 + (1.0/(1.0+np.linalg.norm(np.asarray(test_conf,dtype=float)-np.asarray(item,dtype=float))))
for item in confabs_s2:
	numabs_s2 = numabs_s2 + (1.0/(1.0+np.linalg.norm(np.asarray(test_conf2,dtype=float)-np.asarray(item,dtype=float))))

num_s2 = 2*num_s2 / ((len(conf_s2)+1)*(len(conf_s2)))
numabs_s2 = 2*numabs_s2 / ((len(confabs_s2)+1)*(len(confabs_s2)))

print num_s1,num_s2
print "\n"

if num_s1<num_s2:
	print "Place VM in Server 1 (Clustering Decision)\n"

else:
	print "Place VM in Server 2 (Clustering Decision)\n"

print "\n"
print numabs_s1,numabs_s2

if numabs_s1<numabs_s2:
	print "Place VM in Server 1 (Absolute Decision)\n"

else:
	print "Place VM in Server 2 (Absolute Decision)\n"		

