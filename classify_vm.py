import csv,re,os,sys
import numpy as np
import skfuzzy as fuzz

def classify_vm(filepath):
	vm = []
	vm_abs = []
	csv_file = open(filepath,'rb',os.O_NONBLOCK)
	spamreader = csv.reader(csv_file, delimiter=',', quotechar='|')
	for row in spamreader:
		row1= []
		row2= []
		count1 = 0
		total_memory=1
		for item in row:
			if count1==9 or  count1==10 or count1==16 or count1==7 or count1==8:
				count1 = count1 +1
				continue
			elif count1==11:
				total_memory = float(item)
			elif count1==12  or  count1==17  or  count1==18  or  count1==19:
				row1.append((float(item)/total_memory)*100)
				if count1==12:
					row2.append(float(item))
			else:
				row1.append(float(item))
				if count1==0 or count1==1 or count1==5 or count1==6 or count1==13:
					row2.append(float(item))
			count1 = count1 + 1
		row=row1
		vm.append(row)
		vm_abs.append(row2)
	csv_file.close()


	k_max = [0 for x in xrange(len(vm[0]))]
	k_max[0]=150000000.0
	k_max[1]=150000000.0
	k_max[2]=100.0
	k_max[3]=100.0
	k_max[4]=100.0
	k_max[5]=1000000000.0
	k_max[6]=1000000000.0
	k_max[7]=100.0
	k_max[8]=100.0
	k_max[9]=100.0
	k_max[10]=100.0
	k_max[11]=100.0
	k_max[12]=100.0
	k_max[13]=100.0

	i=0
	while i<len(vm):
		vm[i]=[((float(vm[i][j])*100)/(k_max[j])) for j in xrange(0,len(vm[i]))]
		i=i+1
	testX = np.asarray(vm,dtype=float)

	#Read pre calculated centers of clusters from cluster.csv
	cntrs = []
	csv_file = open('cluster.csv','rb')
	spamreader = csv.reader(csv_file, delimiter=',', quotechar='|')
	for row in spamreader:
		cntrs.append(row)
	cntrs = np.asarray(cntrs,dtype=float)
	csv_file.close()



	u, u0, d, jm, p, fpc = fuzz.cluster.cmeans_predict(testX.T, cntrs, 2, error=0.005, maxiter=1000)
	cluster_membership = np.argmax(u, axis=0)
	i =0
	u = u.T
	conf = np.zeros(6,dtype=float)
	count = 0

	while i <u.shape[0]:
		conf = conf + u[i]
		count = count + 1
		i = i+1
	conf = conf/count

	count = 0
	i=0
	vm_abs = np.asarray(vm_abs)
	conf2 = np.zeros(vm_abs.shape[1],dtype=float)
	while i<vm_abs.shape[0]:
		conf2 = conf2 + vm_abs[i]
		count = count+1
		i=i+1
	conf2 = conf2/count
	
	return conf,conf2


